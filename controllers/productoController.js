let Producto= require('../models/producto');//importamos el modelo producto

function welcome (req,res){ //funcion welcome que reciba una solicitud(req) y genere una respuesta(res)

    res.status(200).send(//toma la respuesta, pone el estatus 200 y envia un json con message: "Welcome!"
        {
            message: "Welcome!"
        }
    );
}

//FUNCION PARA GUARDAR
function saveProducto(req,res){ //creamos una funcion para guardar, que reciba una solicitud(req) y genere una respuesta(res)

    let myProducto = new Producto (req.body);//Cree un nuevo objeto tipo Producto, con el cuerpo de la solictitud(req)
    //Linea para Guardar con token
    //myProducto.usuario=req.payload.email;//el usuario de mi producto es el username del req.data(Datos de user en modelo user)
                                        //al campo Ususario Agregele el Email de la verificacion
    
    myProducto.save((err, result)=>{//el metodo save devuelve una promesa, entonces podenomos que devulve un error(err) o un resultado(result)
        if(err){
            res.status(500).send({ message: err});
            console.log(result)
            console.log(err)//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: "Producto agregado", result:result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

//FUNCION PARA LISTAR
function listProducto(req,res){

    let query = Producto.find({}).sort('fecha');//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}


//FUNCION PARA LISTAR POR ID
function findProducto(req,res){

    let id =req.params.id;//busque id en los parametros de la solicitud

    let query = Producto.findById(id);//consulta por categoria

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }

    });
}

//FUNCION PARA BUSCAR PRODUCTO
function buscarProducto(req,res){

    let search =req.params.search;//busque un parametro "search" en la solicitud
    let queryParam={};//se declara vacio

    if(search){//si existe un search, entonces queryParam tiene un valor

        queryParam={
            $or:[
                {nombreProducto:{ $regex: search, $options:"i"} },//busque por coincidencia(Regex) en nombreProdcuto y sin inportar mayuscula o minuscula($options:"i")
                {categoria:{ $regex: search, $options:"i"} },
                {animal:{ $regex: search, $options:"i"} },
                {descripcion:{ $regex: search, $options:"i"} }
                ]
            };
    };

    query = Producto.find(queryParam);

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

//FUNCION PARA ACTUALIZAR PRODUCTO
function updateProducto(req,res){

    let id = req.params.id;//busque id en los parametros de la solicitud
    let data= req.body;//traigame el cuerpo o estrutura de los datos
    //myProducto.usuario=req.payload.email;//Para guardar con token

    Producto.findByIdAndUpdate(id, data,{new: true},(err, result)=>{//la funcion findByIdAndUpdate busca y actualiza por id, pide id y data(estructura);{new: true} es porfa devulavame el dato nuevo

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message:"Producto actualizado", result:result });//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

//FUNCION PARA BORRAR PRODUCTO
function deleteProducto(req,res){

    let id = req.params.id;//busque id en los parametros de la solicitud

    Producto.findByIdAndDelete(id,(err,result)=>{// funcion findByIdAndDelete busca y elimina por id, retorna una promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message:"Producto Eliminado",result:result });//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });

}

//FUNCIONES PARA LISTAR POR CATEGORIA-----------------------------------------------------------------------

//Funcion para COMIDA
function ProdCatComida(req,res){

    //let cat =req.params.categoria;

    let query = Producto.find({"categoria":"Comida",$options:"i"});//listeme los productos y ordenelos por fecha
    
    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(420).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}
//Funcion para LIMPIEZA
function ProdCatLimpieza(req,res){

    //let cat =req.params.categoria;
    
    let query = Producto.find({"categoria":"Limpieza"});;//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(421).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}


//Funcion para ACCESORIOS
function ProdCatAccesorios(req,res){

    //let cat =req.params.categoria;
    
    let query = Producto.find({"categoria":"Accesorios"});//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(422).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}


//FUNCIONES PARA LISTAR POR ANIMALES-----------------------------------------------------------------------

//Funcion para PERRO
function ProdAnimPerro(req,res){

    let query = Producto.find({$options:"i","animal":"Perro"});//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(520).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}
function ProdAnimGato(req,res){

    let query = Producto.find({"animal":"Gato"});//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(530).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}
function ProdAnimOtros(req,res){

    let query = Producto.find({"animal":"Otros"});//listeme los productos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(540).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

module.exports = {welcome,saveProducto,listProducto,ProdCatComida,ProdCatLimpieza,ProdCatAccesorios,ProdAnimPerro,ProdAnimGato,ProdAnimOtros,findProducto,buscarProducto,updateProducto,deleteProducto}; //exporteme la funcion welcoma para que pueda ser usado en todas partes
//module.exports = {saveProducto};//exporteme la funcion saveProducto para que pueda ser usado en todas partes