const Usuario = require('../models/user');//importamos el modelo Usuario
const bcrypt = require('bcrypt');//Importa libreria para codificar contraseña
//FUNCION PARA GUARDAR
async function saveUsuario(req,res){ //creamos una funcion para guardar, que reciba una solicitud(req) y genere una respuesta(res)
//funcion asincrona es para qu espere las solicitudes

    let myUsuario = new Usuario (req.body);//Cree un nuevo objeto tipo Usuario, con el cuerpo de la solictitud(req)

    //Para no Registrar un usuario con el mismos correo
    const userFound = await Usuario.findOne(
        {
            email:myUsuario.email
        }
    );

    if(userFound != null){
        res.status(403).send({ message: 'El Usuario ya esta Registrado'});
    }else{
    
        //Para Codificar o encrptar una contraseña
    //generar un salt para un hash password

    //salt es el numero que se le agrega de mas a una constraseña para que sea mas larga
    //hash es un algoritma que cifra la contraseña; justo con el salt cifra una contraseña mas laraga lo que da mas seguridad

    const salt = await bcrypt.genSalt(10);//quiero que "const salt" espere y genere un salt

    myUsuario.password= await bcrypt.hash(myUsuario.password, salt)//espere a que que con el password y salt generen un hash para el cifrado

    myUsuario.save((err, result)=>{//el metodo save devuelve una promesa, entonces podenomos que devulve un error(err) o un resultado(result)
        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
    }
    
}

module.exports = {saveUsuario};
