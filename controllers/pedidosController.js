let Pedidos= require('../models/pedidos');//importamos el modelo pedidos

//FUNCION PARA LISTAR
function listPedidos(req,res){

    let query = Pedidos.find({}).sort('fecha');//listeme los pedidos y ordenelos por fecha

    query.exec((err, result)=>{//para ejecutar la consulta, no genera resultado sinonuna promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }

    });

}

//FUNCION PARA GUARDAR
function savePedidos(req,res){ //creamos una funcion para guardar, que reciba una solicitud(req) y genere una respuesta(res)

    let myPedido = new Pedidos (req.body);//Cree un nuevo objeto tipo Pedidos, con el cuerpo de la solictitud(req)
    
    //Linea para Guardar con token
    //myProducto.usuario=req.data.username;//el usuario de mi producto es el username del req.data(Datos de user en modelo user)

    myPedido.save((err, result)=>{//el metodo save devuelve una promesa, entonces podenomos que devulve un error(err) o un resultado(result)
        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message:"Pedido Agregado",result:result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

//FUNCION PARA ACTUALIZAR PRODUCTO
function updatePedidos(req,res){

    let id = req.params.id;//busque id en los parametros de la solicitud
    let data= req.body;//traigame el cuerpo o estrutura de los datos

    Pedidos.findByIdAndUpdate(id, data,{new: true},(err, result)=>{//la funcion findByIdAndUpdate busca y actualiza por id, pide id y data(estructura);{new: true} es porfa devulavame el dato nuevo

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message: result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });
}

//FUNCION PARA BORRAR PRODUCTO
function deletePedidos(req,res){

    let id = req.params.id;//busque id en los parametros de la solicitud

    Pedidos.findByIdAndDelete(id,(err,result)=>{// funcion findByIdAndDelete busca y elimina por id, retorna una promesa

        if(err){
            res.status(500).send({ message: err});//si devulve un error muestre estatus 500 y un mensaje con el error
        }else{
            res.status(200).send({ message:"Pedido Eliminado",result:result});//si devulve un resultado muestre estatus 200 y un mensaje con el resultado
        }
    });

}

module.exports = {listPedidos,savePedidos,updatePedidos,deletePedidos}; //exporteme la funcion welcoma para que pueda ser usado en todas partes
//module.exports = {savePedidos};//exporteme la funcion saveProducto para que pueda ser usado en todas partes