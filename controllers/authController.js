//Metodos de la Autentificacion

const jwt = require("jsonwebtoken");
const Usuario = require("../models/user");
const bcrypt = require('bcrypt');

//FUNCION PARA HACER LOGIN

async function login (req,res){//funcion asincronica login que reciba una solicitud(req) y genere una respuesta(res)
//funcion asincrona es para qu espere las solicitudes

    const user = await Usuario.findOne({//validar si el usuario existe, el await devuelve una promesa
        email:req.body.email//busque en la solicitud un username que sea como req.body.username
    });

    if(user==null){//si el usuario es nulo o no pasa la comparacion da "Credencial Invalida"
        res.status(403).send({ message:"Usuario Incorrecto"});
        return;//se pone un return para que se salga del if
    }else{

        //para comparar la contraseña digita vs la de la base de datos
        const validPassword= await bcrypt.compare(req.body.password,user.password);//compare req.body.password(clave ingresada en la solicitud) con user.password(clave en la BD) y guarde eso en  "const validPassword" 

        if(!validPassword){//si validPassword es falso haga
            res.status(403).send({ message: "Contraseña Incorrecta"});
        }else{
            let token = await new Promise((resolve,reject)=>{//sino en diferente haga y espere una promesa donde 

                jwt.sign(user.toJSON(),'secretKey',{expiresIn:'1d'},(err, token)=>{//la funcion crea un callback (err, token)=>{}//{espiresIn:'1d'} propiedad para que el token expire en 1d (un dia)
                    if(err){
                        reject(err);
                    }else{
                        resolve(token);
                    }
                });
            });
            res.status(200).send({message:"Autenticacion Exitosa", token:token});// envie estatus 200 y muestre el token
            return;//se pone un return para que se salga del if
        }};
    }

function verifyToken (req,res,next){//funcion verifyToken que reciba una solicitud(req) y genere una respuesta(res), que se ejecute y siga (next) pues es un metodo intermediario

    const requestHeader = req.headers['authorization'];//crea una constaste que guarda los header de la solicitud y ahi busque un valor llamado "authorization#

    if(typeof requestHeader !== 'undefined'){//si el valor de requestHeader NO es indefinido 
        
        const token =requestHeader.split(" ")[1];//el split se usa para separar el texto por (" ") y crea un arrayy seleccione el elemeneto [1] del array
        /*
        [Bearer , eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsImlhdCI6MTY2NjY2MzQzMH0.IHyOpyDuQT3IifHVvBSod4Fca1AkRusp3kx6Sx2gR7E]
        
        [ elemento  0, elemento 1]
        */
        
        jwt.verify(token,'secretKey',(err, payload)=>{//verifique y compare el token y la "secretKey" del postman con el de la funcion login, devuelve un callback//el payload es donde se guarad el json con los datos del usurio
            if(err){//si la verificacion de error
                res.status(403).send({ testResult:"token no Corresponde"});//si devulve un error muestre estatus 403 
            }else{
                req.payload = payload;//sino a la solicitud agregre una enbezado nuevo
                next();//vayase al siguiente metodo
            }
        });
        
    }else{
        res.status(403).send({ testResult:"token no Enviado"});//si es indefinido envie estatus 403
    }
}

//FUNCION PARA HACER PRUEBAS DE AUTENTICACION
function profile (req,res){//funcion test que reciba una solicitud(req) y genere una respuesta(res)
    
    res.status(200).send({message: req.payload});
    //res.status(200).send({testResult:"token es valido"})
}

module.exports = {login,profile,verifyToken}//exporteme las funciones para que puedan ser usado en todas partes