const express = require('express');//Necesito el paquete o extencion llamado express y guardemelo en el objeto llamado express
//---(objeto )---------(extencion  )

const router = require('./routers/router');//importeme el router.js de la direccion './routers/router'
let app=express();
//creeme una app con la estructura del paquete express

//agregamos un middleware, es una capa que agrega a las apps para que este pendiente de algo

app.use(express.json());
//que la app use el middleware express.json(), para que las peticiones que sean tipo json me las pase a json

app.use(express.urlencoded({extended:true}));
//middleware, para que las rutas de express puedan tener parametros

//Metadatos de las respuestas generadas de las respuestas de las solicitudes

app.use((req, res, next) => {//las respuesta que tiene esta app tiene los siguientes encabezados(header)
    res.header('Access-Control-Allow-Origin', '*');//las respuestas de la app deselas al servidor que las pida
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');//autorizaciones 
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');//Permita solicitudes tipo HTTP
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();});

app.use(router);//esta aplicacion va usar ese router

module.exports = app; //exporteme el obejeto app para que pueda ser usado en todas partes

