
const { Router } = require('express');//del paquete o extencion llamado express traigame la interfaz Router
const productoController = require('../controllers/productoController');//importeme productoController de la ruta '../controllers/productosController' y guardemelo en const productoController
const authController = require('../controllers/authController');//importeme authController
const userController = require('../controllers/userController');//importeme userController
const pedidosController = require('../controllers/pedidosController');//importeme pedidosController de la ruta '../controllers/pedidosController' y guardemelo en const pedidosController

let router = Router();//el objeto router es de la clase Router()
/*esto implica que el objeto router va tenre todas las caracteristicas de la clase Router()

router.get
router.post
router.delete
router.patch
router.options
*/
/*
router.get('/',(req,res)=> {              //acepte una solicitud tipo get en esa ruta,(req,res)=> haga una funcion que acepte las solicitudes(req) y de una respuesta(res) 
    productoController.welcome(req,res);    //vaya a paperController y llame la funcion welcome y pase la solicitud y respuesta que recibio
}); */     

//KISS= keep it simple, stupid

//RUTAS PRODUCTO
router.get('/',productoController.welcome); //ruta para que llame la funcion welcome--prueba--
router.post('/producto/save', productoController.saveProducto);//ruta para guardar datos con el metodo html post 
router.get('/producto/list',productoController.listProducto)//Ruta para que liste todos los productos
router.get('/producto/:id',productoController.findProducto)//Ruta para que liste los productos por id
router.get('/producto/list/:search',productoController.buscarProducto)//Ruta para que busque un productos por teclado
router.put('/producto/:id',productoController.updateProducto)//Ruta para que actualice los datos de un producto por id
router.delete('/producto/:id',productoController.deleteProducto)//Ruta para que borre un producto por id

//RUTAS PRODUCTO por CATEGORIAS
router.get('/producto/list/cat/comida',productoController.ProdCatComida)//Ruta para que liste productos por categoria Comida
router.get('/producto/list/cat/limpieza',productoController.ProdCatLimpieza)//Ruta para que liste productos por categoria Limpieza
router.get('/producto/list/cat/accesorios',productoController.ProdCatAccesorios)//Ruta para que liste productos por categoria Accesorios

//RUTAS PRODUCTO por Animal
router.get('/producto/list/anim/perro',productoController.ProdAnimPerro)//Ruta para que liste productos por categoria Comida
router.get('/producto/list/anim/gato',productoController.ProdAnimGato)//Ruta para que liste productos por categoria Limpieza
router.get('/producto/list/anim/otros',productoController.ProdAnimOtros)//Ruta para que liste productos por categoria Accesorios


//RUTAS AUTENTICACION
router.post( '/auth/login',authController.login);//Ruta para hacer login
router.post( '/auth/verify', authController.verifyToken,authController.profile);//Ruta para hacer hacer verificacion y pruebas de autenticacion


//GUARDAR CON AUTENTICACION
//router.post('/producto/save', productoController.saveProducto);//ruta para guardar datos con el metodo html post 
//router.post( '/producto/login/save',authController.verifyToken,productoController.saveProducto);//ruta para guardar datos con verificacion de login 
//router.put('/producto/:id',authController.verifyToken,productoController.updateProducto)//Ruta para que actualice los datos de un producto por id, con verificacion de login 
//router.delete('/producto/:id',authController.verifyToken,productoController.deleteProducto)//Ruta para que borre un producto por id, con verificacion de login 

//RUTAS USUARIO/producto/login/save
router.post( '/user/save',userController.saveUsuario);//Ruta para guardar un usuario

//RUTAS PEDIDOS
router.post( '/pedidos/save', pedidosController.savePedidos);//ruta para guardar pedidos con el metodo html post 
router.get('/pedidos/list',pedidosController.listPedidos);//Ruta para que liste todos los pedidos
router.put('/pedidos/:id',pedidosController.updatePedidos);//Ruta para que actualice los datos de un pedido por id
router.delete('/pedidos/:id',pedidosController.deletePedidos);//Ruta para que borre un pedido por id

module.exports=router;//exporteme el objeto router para que pueda ser usado en todas partes
