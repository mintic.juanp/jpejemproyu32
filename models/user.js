const mongoose = require('mongoose');//Necesito el paquete o extencion llamado mongoose y guardemelo en el objeto llamado mongoose
//---(objeto )---------(extencion  )

let Schema = mongoose.Schema; //traigame de la libreria mongoose la clase Schema

const usuarioSchema = Schema(//creeme una contante que guarde el esqueme de un usuario
    {
        name:{type:String},
        email: {type:String, required:true, unique: true},
        password:{type:String, required:true}
    },
    {
        timestamps:true//crea fecha y hora de creacion y actualizacion
    }
);

const Usuario = mongoose.model('usuario',usuarioSchema);//cree una constante llamada Usuario que es un modelo de mongoose, que se comunica con una tabla llamada 'Usuario' y tiene un esquema usuarioSchema

module.exports = Usuario; //exporteme el objeto Usuario para que pueda ser usado en todas partes