const mongoose = require('mongoose');//Necesito el paquete o extencion llamado mongoose y guardemelo en el objeto llamado mongoose
//---(objeto )---------(extencion  )

let Schema = mongoose.Schema; //traigame de la libreria mongoose la clase Schema

const productoSchema = Schema(//creeme una contante que guarde el esqueme de un producto
    {
        nombreProducto:{type:String, required:true},
        urlImage:{type:String, required:true},
        categoria: {type:String, required:true},
        animal:{type:String, required:true},
        descripcion:{type:String,required:true},
        precio:{type:Number, required:true},
        usuario:{type:String},
        fecha: {type:Date, default: Date.now} //fecha tipo date, si no se ingresa valor ponga la fecha de hoy
    }
);

const Producto = mongoose.model('producto',productoSchema);//cree una constante llamada Producto que es un modelo de mongoose, que se comunica con una tabla llamada 'producto' y tiene un esquema productoSchema

module.exports = Producto; //exporteme el objeto Producto para que pueda ser usado en todas partes