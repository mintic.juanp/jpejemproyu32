const mongoose = require('mongoose');//Necesito el paquete o extencion llamado mongoose y guardemelo en el objeto llamado mongoose
//---(objeto )---------(extencion  )

let Schema = mongoose.Schema; //traigame de la libreria mongoose la clase Schema

const pedidosSchema = Schema(//creeme una contante que guarde el esqueme de un pedidos
    {
        nombreProducto:{type:String, required:true},
        precioProducto:{type:Number, required:true},
        cantidadProducto:{type:Number, required:true},
        nombreCliente:{type:String, required:true},
        cedula: {type:String, required:true},
        ciudad: {type:String, required:true},
        direccion: {type:String, required:true},
        telefono:{type:String, required:true},
        email:{type:String, required:true},
        fechaCompra: {type:Date, default: Date.now} //fecha tipo date, si no se ingresa valor ponga la fecha de hoy
    }
);

const Pedidos = mongoose.model('pedidos',pedidosSchema);//cree una constante llamada Pedidos que es un modelo de mongoose, que se comunica con una tabla llamada 'pedidos' y tiene un esquema pedidosSchema

module.exports = Pedidos; //exporteme el objeto Pedidos para que pueda ser usado en todas partes